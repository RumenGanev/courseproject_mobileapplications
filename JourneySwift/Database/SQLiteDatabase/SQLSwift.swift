//
//  SQLSwift.swift
//  Journey
//
//  Created by Rumen Ganev on 24.11.21.
//

import Foundation
import SQLite3

enum SQLiteError: Error {
  case OpenDatabase(message: String)
  case Prepare(message: String)
  case Step(message: String)
  case Bind(message: String)
}

class SQLiteDatabase: ObservableObject {
  @Published var locations: [LocationSQL] = []
    
  private let dbPointer: OpaquePointer?
    
    var errorMessage: String {
      if let errorPointer = sqlite3_errmsg(dbPointer) {
        let errorMessage = String(cString: errorPointer)
        return errorMessage
      } else {
        return "No error message provided from sqlite."
      }
    }

    static let directoryUrl = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)

    static var path: String? {
      return directoryUrl?.appendingPathComponent("database.sqlite").relativePath
    }
    
  private init(dbPointer: OpaquePointer?) {
    self.dbPointer = dbPointer
    locations = getAllLocations() ?? []
  }
  deinit {
    sqlite3_close(dbPointer)
  }
    
    static func open(path: String) throws -> SQLiteDatabase {
      var db: OpaquePointer?
        if sqlite3_open(path, &db) == SQLITE_OK {
            return SQLiteDatabase(dbPointer: db)
      } else {
        defer {
          if db != nil {
            sqlite3_close(db)
          }
        }
        if let errorPointer = sqlite3_errmsg(db) {
          let message = String(cString: errorPointer)
          throw SQLiteError.OpenDatabase(message: message)
        } else {
          throw SQLiteError
            .OpenDatabase(message: "No error message provided from sqlite.")
        }
      }
    }

    
}

extension SQLiteDatabase {
 func prepareStatement(sql: String) throws -> OpaquePointer? {
  var statement: OpaquePointer?
  guard sqlite3_prepare_v2(dbPointer, sql, -1, &statement, nil)
      == SQLITE_OK else {
    throw SQLiteError.Prepare(message: errorMessage)
  }
  return statement
 }
}

protocol SQLTable {
  static var createStatement: String { get }
}

extension LocationSQL: SQLTable {
  static var createStatement: String {
    return """
    CREATE TABLE IF NOT EXISTS Location(
      Id INTEGER PRIMARY KEY NOT NULL,
      locationName CHAR(50) NOT NULL,
      locationDescription CHAR(150) NOT NULL,
      visited INT NOT NULL
    );
    """
  }
}

extension SQLiteDatabase {
  func createTable(table: SQLTable.Type) throws {
    
    let createTableStatement = try prepareStatement(sql: table.createStatement)
    
    defer {
      sqlite3_finalize(createTableStatement)
    }
    
    guard sqlite3_step(createTableStatement) == SQLITE_DONE else {
      throw SQLiteError.Step(message: errorMessage)
    }
    print("\(table) table created.")
  }
}

extension SQLiteDatabase {
  func insertLocation(_ location: LocationSQL) throws {
    let insertSql = "INSERT INTO Location (Id, locationName, locationDescription, visited) VALUES (null, ?, ?, ?);"
    let insertStatement = try prepareStatement(sql: insertSql)
    defer {
      sqlite3_finalize(insertStatement)
    }
      let name: NSString = location.locationName
      let description: NSString = location.locationDescription
      let visited: Int32 = location.visited
    guard
        sqlite3_bind_text(insertStatement, 1, name.utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertStatement, 2, description.utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_int(insertStatement, 3, visited) == SQLITE_OK
      else {
        throw SQLiteError.Bind(message: errorMessage)
    }
    guard sqlite3_step(insertStatement) == SQLITE_DONE else {
      throw SQLiteError.Step(message: errorMessage)
    }
    locations.append(location)
    print("Successfully inserted row.")
  }
}

extension SQLiteDatabase {
    func getLocationById(_ id: Int32) -> LocationSQL? {
    let querySql = "SELECT * FROM Location WHERE Id = ?;"
    guard let queryStatement = try? prepareStatement(sql: querySql) else {
      return nil
    }
    defer {
      sqlite3_finalize(queryStatement)
    }
    guard sqlite3_bind_int(queryStatement, 1, id) == SQLITE_OK else {
         return nil
    }
    guard sqlite3_step(queryStatement) == SQLITE_ROW else {
      return nil
    }
    let id = sqlite3_column_int(queryStatement, 0)
    guard let queryResultCol1 = sqlite3_column_text(queryStatement, 1) else {
      print("Query result is nil.")
      return nil
    }
    guard let queryResultCol2 = sqlite3_column_text(queryStatement, 2) else {
      print("Query result is nil.")
      return nil
    }
    let queryResultCol3 = sqlite3_column_int(queryStatement, 3)
        
    let name = String(cString: queryResultCol1) as NSString
    let description = String(cString: queryResultCol2) as NSString
    let visited = queryResultCol3
      return LocationSQL(id: id, locationName: name, locationDescription: description, visited: visited)
  }
}

extension SQLiteDatabase {
    func getAllLocations() -> [LocationSQL]? {
    
        var locations: [LocationSQL]? = []
        
        let querySql = "SELECT * FROM Location;"
        guard let queryStatement = try? prepareStatement(sql: querySql) else {
          return nil
        }
        defer {
          sqlite3_finalize(queryStatement)
        }
        
        while sqlite3_step(queryStatement) == SQLITE_ROW {
            let id = sqlite3_column_int(queryStatement, 0)
            guard let queryResultCol1 = sqlite3_column_text(queryStatement, 1) else {
              print("Query result is nil.")
              return nil
            }
            guard let queryResultCol2 = sqlite3_column_text(queryStatement, 2) else {
              print("Query result is nil.")
              return nil
            }
            let queryResultCol3 = sqlite3_column_int(queryStatement, 3)
                
            let name = String(cString: queryResultCol1) as NSString
            let description = String(cString: queryResultCol2) as NSString
            let visited = queryResultCol3
            
            locations?.append(LocationSQL(id: id, locationName: name, locationDescription: description, visited: visited))
        }
        self.locations = locations ?? []
        return locations
  }
}

extension SQLiteDatabase {
    func updateLocation(id: Int32, location: LocationSQL) throws {
    let updateSql = "UPDATE Location SET locationName = ?, locationDescription = ?, visited = ? WHERE Id = ?;"
    let updateStatement = try prepareStatement(sql: updateSql)
    defer {
      sqlite3_finalize(updateStatement)
    }
      let name: NSString = location.locationName
      let description: NSString = location.locationDescription
      let visited: Int32 = location.visited
    guard
        sqlite3_bind_text(updateStatement, 1, name.utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(updateStatement, 2, description.utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_int(updateStatement, 3, visited) == SQLITE_OK &&
        sqlite3_bind_int(updateStatement, 4, id) == SQLITE_OK
      else {
        throw SQLiteError.Bind(message: errorMessage)
    }
    guard sqlite3_step(updateStatement) == SQLITE_DONE else {
      throw SQLiteError.Step(message: errorMessage)
    }
    let loc = locations.first(where: { location in
        location.id == id
    })
    if let index = locations.firstIndex(where: { $0.id == loc!.id }) {
        locations[index] = location
    }
    print("Successfully updated row.")
  }
}

extension SQLiteDatabase {
    func deleteLocation(id: Int32) throws {
    let deleteSql = "DELETE FROM Location WHERE Id = ?;"
    let deleteStatement = try prepareStatement(sql: deleteSql)
    defer {
      sqlite3_finalize(deleteStatement)
    }
    guard
        sqlite3_bind_int(deleteStatement, 1, id) == SQLITE_OK
      else {
        throw SQLiteError.Bind(message: errorMessage)
    }
    guard sqlite3_step(deleteStatement) == SQLITE_DONE else {
      throw SQLiteError.Step(message: errorMessage)
    }
        
    let location = locations.first(where: { location in
        location.id == id
    })
    if let index = locations.firstIndex(where: { $0.id == location!.id }) {
        locations.remove(at: index)
    }
    print("Successfully deleted row.")
  }
}
