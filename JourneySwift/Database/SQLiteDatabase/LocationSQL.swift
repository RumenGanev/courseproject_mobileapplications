//
//  LocationSQL.swift
//  Journey
//
//  Created by Rumen Ganev on 24.11.21.
//

import Foundation

struct LocationSQL: Identifiable, Equatable {
    var id: Int32 = 0
    var locationName: NSString = ""
    var locationDescription: NSString = ""
    var visited: Int32 = 0
    
    init(locationName: NSString, locationDescription: NSString, visited: Int32){
        self.locationName = locationName
        self.locationDescription = locationDescription
        self.visited = visited
    }
    
    init(id: Int32, locationName: NSString, locationDescription: NSString, visited: Int32){
        self.id = id
        self.locationName = locationName
        self.locationDescription = locationDescription
        self.visited = visited
    }
}
