//
//  WeatherAPI.swift
//  JourneySwift
//
//  Created by Rumen Ganev on 26.11.21.
//

import Foundation

import Foundation

struct Weather: Decodable {
    
    let cityName: String
    let windSpeed: Double
    let humidity: Double
    let cloudCoverage: Double
    let feelsLike: Double
    
    enum CodingKeys: String, CodingKey {
        case cityName = "name"
        case windSpeed = "wind_kph"
        case humidity = "humidity"
        case cloudCoverage = "cloud"
        case feelsLike = "feelslike_c"
    }
}

extension Weather {
    
    init?(json: [String: Any]) {
        print(json)
        
        guard let location = json["location"] as? [String: Any],
              let name = location["name"],
              let current = json["current"] as? [String: Any],
              let windSpeed = current["wind_kph"],
              let humidity = current["humidity"],
              let cloudCoverage = current["cloud"],
              let feelsLike = current["feelslike_c"]
        else {
            return nil
        }
        
        self.cityName = name as! String
        self.windSpeed = windSpeed as! Double
        self.humidity = humidity as! Double
        self.cloudCoverage = cloudCoverage as! Double
        self.feelsLike = feelsLike as! Double
    }
}

enum FetchError: Error {
    case noData
}

class Requester {
    
    func urlRequest(city: String) -> URL? {

        var cityName = city
        
        if cityName.contains(" ") {
            cityName = city.replacingOccurrences(of: " ", with: "%20")
        }
        
        if let url = URL(string: "https://api.weatherapi.com/v1/current.json?key=7382877cb872416f944102049212611&q=\(cityName)") {
            return url
        }
        
        return nil
    }

    func getWeather(url: URL) async throws -> Weather? {
        
        var weather: Weather?
        
        async let (data, _) = try await URLSession.shared.data(from: url)
        
        do {
            if let json = try await JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                weather = Weather(json: json)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return weather
    }
}
