//
//  JourneySwiftApp.swift
//  JourneySwift
//
//  Created by Rumen Ganev on 25.11.21.
//

import SwiftUI

@main
struct JourneySwiftApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
