//
//  WeatherView.swift
//  JourneySwift
//
//  Created by Rumen Ganev on 28.11.21.
//

import SwiftUI

struct WeatherView: View {
    
    @State var weather: Weather?
    @State var locationName: String = ""
    @State var isDataActive: Bool = false
    
    
    var name: String {
        if weather != nil {
            return weather?.cityName ?? ""
        }
        return ""
    }
    var wind: String {
        if weather != nil {
            return String(format: "%.2f", weather?.windSpeed as! CVarArg)
        }
        return ""
    }
    var humidity: String {
        if weather != nil {
            return String(format: "%.0f", weather?.humidity as! CVarArg)
        }
        return ""
    }
    var clouds: String {
        if weather != nil {
            return String(format: "%.0f", weather?.cloudCoverage as! CVarArg)
        }
        return ""
    }
    var feelsLike: String {
        if weather != nil {
            return String(format: "%.1f", weather?.feelsLike as! CVarArg)
        }
        return ""
    }
    
    var body: some View {
        Form {
            Section {
                TextField(
                    "Location Name",
                    text: $locationName,
                    prompt: Text("Name of the location")
                        .font(.title3)
                )
                    .padding(.horizontal, 15)
                    .border(.black, width: 3)
                
                Button("Get current weather", action: {
                    weatherRequest()
                    isDataActive = true
                })
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity)
                    .frame(height: 35)
                    .background(Color.blue)
                    .border(.black, width: 3)
            } header: {
                Text("Location:")
            }
                
            Section {
                Text("Location: \(name)")
                Text("Wind: \(wind) km/h")
                Text("Humidity: \(humidity)%")
                Text("Cloud coverage: \(clouds)%")
                Text("Feels like: \(feelsLike)°")
            } header: {
                Text("Weather:")
            }
            
        }
        .navigationBarTitle("Weather")
    }
    
    func weatherRequest() {
        let req = Requester()
        
        guard let request = req.urlRequest(city: locationName) else {
            print("No url")
            return
        }
        
        Task {
            do {
                weather = try await req.getWeather(url: request)
            } catch let error {
                print(error)
            }
        }
    }
}

struct WeatherView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherView()
    }
}
