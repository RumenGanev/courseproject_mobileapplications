//
//  TestView.swift
//  Journey
//
//  Created by Rumen Ganev on 24.11.21.
//

import SwiftUI

struct TestView: View {
    
    var db = try! SQLiteDatabase.open(path: SQLiteDatabase.path ?? "")

    var body: some View {
        VStack {
            Button("Test", action: {
                create()
                ins()
                //check()
                //update()
                //check()
                //delete()
                //check()
            })
        }
    }
    
    func create() {
        do {
          try db.createTable(table: LocationSQL.self)
        } catch {
          print(db.errorMessage)
        }
    }
    func ins() {
        let loc = LocationSQL(locationName: "Sofia", locationDescription: "Capital of Bulgaria", visited: 1)
        do {
          try db.insertLocation(loc)
        } catch {
          print(db.errorMessage)
        }
    }
    func update(){
        let loc = LocationSQL(locationName: "Yambol", locationDescription: "Home city", visited: 1)
        do{
            try db.updateLocation(id: 1, location: loc)
        } catch {
            
        }
    }
    func delete(){
        do{
            //try db.deleteLocation(id: 2)
        } catch {
            
        }
    }
    func check() {
        let locs = db.getAllLocations()
        print(locs)
    }

}

struct TestView_Previews: PreviewProvider {
    static var previews: some View {
        TestView()
    }
}
