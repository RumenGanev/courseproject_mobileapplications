//
//  AddJourneyView.swift
//  Journey
//
//  Created by Rumen Ganev on 24.11.21.
//

import SwiftUI

struct UpdateJourneysView: View {
    
    let model: SQLiteDatabase
    var id: Int32
    
    @Environment(\.presentationMode) var presentationMode
    @State var locationName: String = ""
    @State var locationDescription: String = ""
    @State var visited: Int = 0
    
    var body: some View {
        VStack(spacing: 15) {
            TextField(
                "Location Name",
                text: $locationName,
                prompt: Text("Name of the location")
                    .font(.title3)
            )
                .padding(.horizontal, 15)
                .border(.black, width: 3)

            TextField(
                "Location Name",
                text: $locationDescription,
                prompt: Text("Description of the location")
                    .font(.title3)
            )
                .lineLimit(5)
                .padding(.horizontal, 15)
                .border(.black, width: 3)
            
            Picker("Already visited", selection: $visited) {
                Text("Visited").tag(1)
                Text("Not visited").tag(0)
            }
            .pickerStyle(.segmented)
            
            HStack(spacing: 15) {
                Button("Back", action: {
                    self.presentationMode.wrappedValue.dismiss()
                })
                    .foregroundColor(.black)
                    .frame(width: 110, height: 50)
                    .border(.black, width: 2)
                Button("Change", action: {
                    try! model.updateLocation(id: id, location: LocationSQL(locationName: locationName as NSString, locationDescription: locationDescription as NSString, visited: Int32(visited)))
                    
                    self.presentationMode.wrappedValue.dismiss()
                })
                    .foregroundColor(.white)
                    .frame(width: 110, height: 50)
                    .background(Color.blue)
                    .border(.black, width: 2)
            }
        }
        .padding()
        .navigationBarTitle("Change Location")
        .navigationBarBackButtonHidden(true)
    }
}

struct UpdateJourneysView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateJourneysView(model: try! SQLiteDatabase.open(path: SQLiteDatabase.path ?? ""), id: 1)
    }
}
