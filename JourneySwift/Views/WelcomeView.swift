//
//  WelcomeView.swift
//  Journey
//
//  Created by Rumen Ganev on 23.11.21.
//

import SwiftUI

enum Type {
    case journeys
    case weather
}

struct WelcomeView: View {
    
    var body: some View {
        NavigationView{
            VStack(spacing: 15){
                
                NavigationLink(destination: JourneysView()) {
                    imageHeader(type: .journeys)
                        .padding(.horizontal, 20)
                }
                
                NavigationLink(destination: WeatherView()) {
                    imageHeader(type: .weather)
                        .padding(.horizontal, 20)
                }

            }
        }
    }
    
    @ViewBuilder
    func imageHeader(type: Type) -> some View {
        switch type {
            case .journeys:
                ZStack {
                    HStack {
                        Text("Journeys")
                            .font(.custom("Georgia-Italic", size: 30))
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .shadow(color: .black ,radius: 1)
                        Spacer()
                        Image(systemName: "greaterthan")
                            .resizable()
                            .tint(.white)
                            .frame(width: 15, height: 15)
                            .shadow(color: .black ,radius: 1)
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.horizontal, 20)
                .padding(.vertical, 30)
                .background(Image("image.road"), alignment: .bottom)
                .cornerRadius(22)
            case .weather:
                ZStack {
                    HStack {
                        Text("Weather")
                            .font(.custom("Georgia-Italic", size: 30))
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .shadow(color: .black ,radius: 1)
                        Spacer()
                        Image(systemName: "greaterthan")
                            .resizable()
                            .tint(.white)
                            .frame(width: 15, height: 15)
                            .shadow(color: .black ,radius: 1)
                    }
                    
                }
                .frame(maxWidth: .infinity)
                .padding(.horizontal, 20)
                .padding(.vertical, 30)
                .background(
                    Image("image.weather"),
                    alignment: .topTrailing
                )
                .cornerRadius(22)
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
