//
//  JourneysView.swift
//  Journey
//
//  Created by Rumen Ganev on 23.11.21.
//

import SwiftUI

struct JourneysView: View {
    
    @StateObject var db = try! SQLiteDatabase.open(path: SQLiteDatabase.path ?? "")
    
    var body: some View {
        
        List{
            ForEach(db.locations) { item in
                NavigationLink(
                    destination: UpdateJourneysView(
                            model: db,
                            id: item.id,
                            locationName: String(item.locationName),
                            locationDescription: String(item.locationDescription),
                            visited: Int(item.visited)
                ))
                {
                    label(item: item)
                }
            }
            .onDelete { idxSet in
                idxSet.forEach { idx in
                    guard db.locations.count > idx else {
                        return
                    }
                    let item = db.locations[idx]
                    try? db.deleteLocation(id: item.id)
                }
            }
        }
        .navigationBarTitle("Journey Planner")
        .navigationBarItems(trailing: NavigationLink(destination: NewJourneyView(model: db)) {
            Image(systemName: "plus")
        })
    }
    
    @ViewBuilder
    func label(item: LocationSQL) -> some View {
        VStack(alignment: .leading) {
            Text(
                String(item.locationName) as String + (item.visited == 1 ? " (Visited)" : " (Not Visited)")
            )
                .bold()
            
            Text(String(item.locationDescription) as String)
        }
    }
}

struct JourneysView_Previews: PreviewProvider {
    static var previews: some View {
        JourneysView()
    }
}
